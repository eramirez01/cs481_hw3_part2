﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FoodPage : ContentPage
    {
        public FoodPage()
        {
            InitializeComponent();
        }

        protected async void On_Appearing(object sender, EventArgs e)
        {
            await DisplayAlert("Hi there", "My favorite foods", "OK");
        }

        protected async void On_Disappearing(object sender, EventArgs e)
        {
            await DisplayAlert("Leaving?", "That sucks", "OK");
        }

    }
}