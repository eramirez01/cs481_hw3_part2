﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HobbyPage : ContentPage
    {
        public HobbyPage()
        {
            InitializeComponent();
        }

        protected async void On_Appearing(object sender, EventArgs e)
        {
            await DisplayAlert("Hi", "These are my hobbies", "OK");
        }

        protected async void On_Disappearing(object sender, EventArgs e)
        {
            await DisplayAlert("Bye now", "Looking for something else?", "OK");
        }
    }
}