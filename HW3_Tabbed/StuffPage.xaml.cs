﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StuffPage : ContentPage
    {
        public StuffPage()
        {
            InitializeComponent();
        }
        protected async void On_Appearing(object sender, EventArgs e)
        {
            await DisplayAlert("Yo", "This is my life. :(", "OK");
            //School = Color.Blue;
            //await school.RelRotateTo(360);
        }

        protected async void On_Disappearing(object sender, EventArgs e)
        {
            await DisplayAlert("Leaving?", "Go for it", "OK");
        }
    }
}